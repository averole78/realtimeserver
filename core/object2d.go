package core

import (
	"realtimeserver/helpers"
	"realtimeserver/helpers/vector"

	"github.com/faiface/pixel/imdraw"
	"golang.org/x/image/colornames"
)

var counterObj2D uint

type IObject2D interface {
	helpers.IElement
	Position() *vector.V
	Radius() float64
	GetID() uint
}

//Object2D Объект верхнего уровня
type Object2D struct {
	ID uint
	*vector.Circle
	next helpers.IElement
}

//NewObject2D Новый объект
func NewObject2D(pos *vector.V, radius float64) *Object2D {
	counterObj2D++
	obj := &Object2D{}
	obj.Circle = vector.NewCircle(pos, radius)
	obj.ID = counterObj2D
	return obj
}

//GetID GetID
func (o *Object2D) GetID() uint {
	return o.ID
}

//GetNext Поддержка IElement
func (o *Object2D) GetNext() helpers.IElement {
	return o.next
}

//SetNext Поддержка IElement
func (o *Object2D) SetNext(p helpers.IElement) {
	o.next = p
}

//Drawing Поддержка IDrawing
func (o *Object2D) Drawing(imd *imdraw.IMDraw, display *vector.Rect) {
	//body
	imd.Color = colornames.Black
	imd.Push(o.Position().ToPixel())
	imd.Circle(o.Radius(), 0.05)
}

//Update Поддержка IUpdate
func (o *Object2D) Update() {
}

//==============================
type Collision struct {
	imp  []*Impulse2D
	cldr *Collider
}
