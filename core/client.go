package core

import (
	"fmt"
	"log"
	"realtimeserver/helpers"
	"realtimeserver/protos"
	"runtime/debug"

	"github.com/golang/protobuf/proto"
	melody "gopkg.in/olahol/melody.v1"
)

type ManagerClients struct {
	clients *helpers.Chain
	g       *GameWorld
}

func NewManagerClients(g *GameWorld) *ManagerClients {
	mc := &ManagerClients{}
	mc.clients = helpers.NewChain()
	mc.g = g
	return mc
}

//NewClient NewClient
func (mc *ManagerClients) NewClient(s *melody.Session) {
	c := NewClient(s, mc.g)
	mc.clients.Set(s, c)
}

//DeleteClient DeleteClient
func (mc *ManagerClients) DeleteClient(s *melody.Session) {
	val, ok := mc.clients.Get(s)
	if ok == true {
		mc.clients.Del(val.(*Client))
		val.(*Client).Disconnect()
	}
}

//ReadClient Чтение данных
func (mc *ManagerClients) ReadClient(s *melody.Session, raw []byte) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("<%s>recover():%s\n", r, debug.Stack())
		}
	}()

	if len(raw) == 0 {
		return
	}
	if val, ok := mc.clients.Get(s); ok == true {
		val.(*Client).UseAPI(FromRaw(raw))
	}
}

//WriteAll Отправление данных всем клиентам
func (mc *ManagerClients) WriteAll(pb proto.Message, m protos.TypeMethod) error {
	raw, err := ToRaw(m, pb)
	if err != nil {
		return err
	}
	mc.clients.Range(func(k, v interface{}) bool {
		err := v.(*Client).WriteRaw(raw)
		if err != nil {
			return false
		}
		return true
	})
	return nil
}

//SendEnemy отправить Врагов клиентам
func (mc *ManagerClients) SendEnemy() {
	// PerSecond := uint(time.Second) / 1
	// var start, now time.Time
	// var delta time.Duration
	// for {
	// 	start = time.Now()
	// 	clients.Range(func(key, value interface{}) bool {
	// 		c := value.(*Client)
	// 		g.pbWorld.Frame = uint32(g.Scene.frame)
	// 		g.pbWorld.Units[c.unit.pb.WID] = c.unit.pb
	// 		return true
	// 	})
	// 	go WriteAll(g.pbWorld, protos.TypeMethod_ENEMY_UPDATE)
	// 	now = time.Now()
	// 	delta = time.Duration(PerSecond) - nog.Sub(start)
	// 	if delta <= 0 {
	// 		fmt.Printf("!!!SendEnemy() delta<0:%+v\n", delta)
	// 		continue
	// 	}
	// 	time.Sleep(delta)
	// }
}

//Client Client
type Client struct {
	*melody.Session
	u *Unit
	g *GameWorld
}

func NewClient(s *melody.Session, g *GameWorld) *Client {
	c := &Client{}
	c.g = g
	c.u = NewUnit(c)
	c.Session = s
	c.g.AddUpdater(c.u)
	c.g.AddDrawing(c.u)
	err := c.Write(c.u.GetProto(), protos.TypeMethod_UNIT_CREATE)
	if err != nil {
		fmt.Printf("Error NewClient->TypeMethod_UNIT_CREATE: %+v\n", err)
	}
	return c
}

//UseAPI Использование клиентских АПИ
func (c *Client) UseAPI(method protos.TypeMethod, body []byte) {
	switch method {
	case protos.TypeMethod_UNIT_ADD:
		// WriteAll(c.unit.Unit, protos.TypeMethod_UNIT_ADD)
		// GameWorld.AddUpdater(c.unit)
		// GameWorld.AddSender(c)

	case protos.TypeMethod_WORLD_LOAD:
		// err := Write(c, GameWorld.GetBufferWorld(), protos.TypeMethod_WORLD_LOAD)
		// if err != nil {
		// 	fmt.Printf("Error UseAPI->TypeMethod_WORLD_LOAD: %+v\n", err)
		// }

	case protos.TypeMethod_INPUT_KEYBOARD:
		kb := &protos.Keyboard{}
		if !Unmarshal(body, kb) {
			return
		}
		c.u.AddKeyBoard(kb)
		fmt.Printf("TypeMethod_INPUT_KEYBOARD: Code:%s, Event:%s\n", kb.Code.String(), kb.Event.String())

	case protos.TypeMethod_INPUT_MOUSE:
		ms := &protos.Mouse{}
		if !Unmarshal(body, ms) {
			return
		}
		c.u.AddMouse(ms)
		fmt.Printf("TypeMethod_INPUT_MOUSE: Code:%s, Event:%s\n", ms.Code.String(), ms.Event.String())
	}
}

//Disconnect Отправление данных 1 клиенту
func (c *Client) Disconnect() {
	c.g.DelUpdater(c.u)
	c.g.DelDrawing(c.u)
	c.Session = nil
	c.g = nil
}

//WriteRaw Запись клиенту
func (c *Client) WriteRaw(raw []byte) error {
	return c.Session.WriteBinary(raw)
}

//Write Отправление данных 1 клиенту
func (c *Client) Write(pb proto.Message, m protos.TypeMethod) error {
	raw, err := ToRaw(m, pb)
	if err != nil {
		return err
	}
	return c.WriteRaw(raw)
}

//Unmarshal Распаковка СЫРЫХ данных
func Unmarshal(buf []byte, pb proto.Message) bool {
	err := proto.Unmarshal(buf, pb)
	if err != nil {
		log.Printf("Ошибка парсинга:%v, %s\n", pb, err)
		return false
	}
	return true
}

//FromRaw Извлекает сообщение из сырых данных ws
func FromRaw(raw []byte) (method protos.TypeMethod, body []byte) {
	method = protos.TypeMethod(raw[len(raw)-1])
	body = raw[:len(raw)-1]
	return method, body
}

//ToRaw Упаковывает сообщение для отправки в ws
func ToRaw(method protos.TypeMethod, pbm proto.Message) (raw []byte, err error) {
	raw, err = proto.Marshal(pbm)
	if err != nil {
		return nil, err
	}
	raw = append(raw, byte(method))
	return raw, err
}
