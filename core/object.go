package core

import (
	"realtimeserver/helpers"
	"realtimeserver/helpers/vector"

	"github.com/faiface/pixel/imdraw"
	"golang.org/x/image/colornames"
)

var countUpdateGrid uint64
var countID uint64

type IUpdater interface {
	Update()
}
type IObject interface {
	Move(delta *vector.V)
	GetPosition() *vector.V
	GetRadius() float64
	GetID() uint64
	GetMass() float64
	GetMaxForce() float64
	GetMaxSpeed() float64
	GetImpulse() *Impulse
	SetImpulse(*Impulse)
	GetVelocity() *vector.V
	SetVelocity(*vector.V)
	Drawing(imd *imdraw.IMDraw, display *vector.Rect)
	Collision(gridCell *vector.V, target *vector.V, radius float64) *Impulse
	GetRose() *vector.Path
}

type Object struct {
	ID uint64
	//particle     *helpers.Particle
	position     *vector.V
	radius       float64 //радиус
	
	mass         float64
	maxForce     float64
	maxSpeed     float64
	rose         *vector.Path
	roseObstacle *vector.Path
	vel          *vector.V //Направленная скорость
	grid         *helpers.Grid
	gridObstacle *helpers.Grid
	impulse      *Impulse
	next         helpers.IElement
}

func (o *Object) GetNext() helpers.IElement {
	return o.next
}
func (o *Object) SetNext(p helpers.IElement) {
	o.next = p
}

func NewObject(grid *helpers.Grid, gridObstacle *helpers.Grid, radius, mass, maxForce, maxSpeed float64) *Object {
	countID++
	o := &Object{}
	o.ID = countID
	o.position = vector.Zero.Copy()
	o.impulse = nil
	o.radius = radius
	o.mass = mass
	o.maxForce = maxForce
	o.maxSpeed = maxSpeed
	//o.particle = helpers.NewParticle(o)
	o.grid = grid
	o.gridObstacle = gridObstacle
	o.rose = o.calcRose(grid)
	o.roseObstacle = o.calcRose(gridObstacle)
	o.Move(grid.Rand())
	return o
}

//Расчитывет проверочные ячейки в сетке
func (o *Object) calcRose(grid *helpers.Grid) *vector.Path {
	cntr := o.GetPosition().Copy().RoundRose(float64(grid.SizeCell()))
	q45 := vector.Unit45.Copy()
	path := &vector.Path{}
	path.AddPoints(
		cntr.Copy().Add(q45),
		cntr.Copy().Add(q45.Rotate90DegLeft()),
		cntr.Copy().Add(q45.Rotate90DegLeft()),
		cntr.Copy().Add(q45.Rotate90DegLeft()))
	return path
}

//Collision Collision
func (o *Object) Collision(gridCell *vector.V, target *vector.V, radius float64) *Impulse {
	imp := &Impulse{}
	//Обходим героев
	o.grid.Range(int(gridCell.GetX()), int(gridCell.GetY()), func(p helpers.IElement) bool {
		imp.Obj = p.(*Object)
		if imp.Obj.GetID() == o.ID {
			imp.Obj = nil
			return true
		}
		imp.Dist = imp.Obj.GetPosition().Copy().Sub(target).Length()
		if imp.Dist < imp.Obj.GetRadius()+radius {
			return false
		}
		imp.Obj = nil
		return true
	})
	// if imp.Obj != nil {
	// 	return imp
	// }
	//Обходим препятствия
	o.gridObstacle.Range(int(gridCell.GetX()), int(gridCell.GetY()), func(p helpers.IElement) bool {
		imp.Obj = p.(*Object)
		if imp.Obj.GetID() == o.ID {
			imp.Obj = nil
			return true
		}
		imp.Dist = imp.Obj.GetPosition().Copy().Sub(target).Length()
		if imp.Dist < imp.Obj.GetRadius()+radius {
			return false
		}
		imp.Obj = nil
		return true
	})
	if imp.Obj != nil {
		return imp
	}
	return nil
}

func (o *Object) GetRose() *vector.Path {
	return o.rose
}

//Move Передвинуть
func (o *Object) Move(delta *vector.V) {
	temp := o.position.Copy()
	o.SetImpulse(nil)
	o.position.Add(delta)

	//Выход за предели сетки
	if !o.grid.Check(int(o.position.GetX()), int(o.position.GetY())) {
		o.position = temp //Возвращаемся
		return
	}

	var imp *Impulse
	//Проходим по rose точкам
	o.rose.Range(func(index int, v *vector.V) bool {
		imp = o.Collision(v, o.GetPosition(), o.GetRadius())
		if imp != nil {
			return false
		}
		return true
	})

	//Столкновение
	if imp != nil && o.GetVelocity() != nil {
		o.position = temp //Возвращаемся
		o.SetImpulse(imp)
		imp.Obj.SetImpulse(&Impulse{Obj: o, Dist: imp.Dist})
	}

	//Смещаемся в сетке
	if int(temp.GetX()) != int(o.position.GetX()) || int(temp.GetY()) != int(o.position.GetY()) {
		o.grid.Del(int(temp.GetX()), int(temp.GetY()), o)
		o.grid.Set(int(o.position.GetX()), int(o.position.GetY()), o)
		countUpdateGrid++
	}
	o.roseObstacle = o.calcRose(o.gridObstacle)
	o.rose = o.calcRose(o.grid)
}

func (o *Object) SetImpulse(imp *Impulse) {
	o.impulse = imp
}
func (o *Object) GetImpulse() *Impulse {
	return o.impulse
}

//GetMass Возвращает массу
func (o *Object) GetMass() float64 {
	return o.mass
}

func (o *Object) GetID() uint64 {
	return o.ID
}
func (o *Object) GetPosition() *vector.V {
	return o.position
}

//GetRadius Радиус объекта
func (o *Object) GetRadius() float64 {
	return o.radius
}

//GetVelocity Возвращает скорость
func (o *Object) GetVelocity() *vector.V {
	return o.vel
}

//SetVelocity Устанавливает скорость
func (o *Object) SetVelocity(v *vector.V) {
	o.vel = v
}

//GetMaxForce Возвращает макс Силу
func (o *Object) GetMaxForce() float64 {
	return o.maxForce
}

//GetMaxSpeed Возвращает макс скорость
func (o *Object) GetMaxSpeed() float64 {
	return o.maxSpeed
}

func (o *Object) Update() {
}

func (o *Object) Drawing(imd *imdraw.IMDraw, display *vector.Rect) {
	//body
	imd.Color = colornames.Black
	imd.Push(o.position.ToPixel())
	imd.Circle(o.radius, 0.05)
	//speed
	imd.Color = colornames.Red
	imd.Push(o.position.ToPixel())
	imd.Push(o.position.Copy().Add(o.vel.Copy().Scale(80)).ToPixel())
	imd.Line(0.1)
	//second radius
	// imd.Color = colornames.Grey
	// imd.Push(o.position.ToPixel())
	// imd.Circle(o.radius*2, 0.05)

	//path
	// imd.Color = colornames.Grey
	// o.path.Range(func(index int, v *vector.V) bool {
	// 	imd.Push(v.ToPixel())
	// 	return true
	// })
	//imd.Line(0.1)
}

type Impulse struct {
	Obj  *Object
	Dist float64
}
