package core

import (
	"math/rand"
	"realtimeserver/helpers/vector"

	"realtimeserver/protos"

	"github.com/faiface/pixel/imdraw"
	"golang.org/x/image/colornames"
)

//Wolf Вражеский волк
type Wolf struct {
	IObject
	sm     *SteeringManager
	target IObject
	g      *GameWorld
}

//Serialize Возвращает прото объект
func (w *Wolf) Serialize() *protos.Enemy {
	pb := &protos.Enemy{}
	pb.ID = uint32(w.GetID())
	pb.Position = w.GetPosition().ToProto()
	pb.Speed = w.GetVelocity().ToProto()
	return pb
}

//NewWolf NewWolf
func NewWolf(g *GameWorld) *Wolf {
	e := &Wolf{}
	e.IObject = NewObject(g.Grid, g.ObstacleGrid, 0.5, rand.Float64()*50+50, 1, 0.05)
	e.g = g
	e.SetVelocity(vector.UnitX.Copy().Truncate(Speed))
	e.sm = NewSteeringManager(e)
	return e
}

//SetTarget Выбирает случайную цель
func (w *Wolf) SetTarget(t IObject) {
	w.target = t
}

//Drawing Рисовать
func (w *Wolf) Drawing(imd *imdraw.IMDraw, display *vector.Rect) {
	//println(w.GetPosition().GetX(), display.Min().GetX())
	if w.GetPosition().GetX() < display.Min().GetX() ||
		w.GetPosition().GetX() > display.Max().GetX() ||
		w.GetPosition().GetY() < display.Min().GetY() ||
		w.GetPosition().GetY() > display.Max().GetY() {
		return
	}

	//Body
	imd.Color = colornames.Rosybrown
	imd.Push(w.GetPosition().ToPixel())
	imd.Circle(0.5, 0)

	//target
	imd.Color = colornames.Black
	imd.Push(w.GetPosition().ToPixel(), w.sm.target.ToPixel())
	imd.Line(0.05)

	//avoidanceForce
	imd.Color = colornames.Royalblue
	imd.Push(w.sm.ahead.ToPixel(), w.sm.ahead.Copy().Add(w.sm.avoidanceForce.Copy().Scale(20)).ToPixel())
	imd.Line(0.5)

	//ahead
	imd.Color = colornames.Yellow
	imd.Push(w.sm.ahead.ToPixel())
	imd.Circle(w.sm.aheadRad, 0.05)

	//imd.Circle(float64(w.g.Grid.SizeCell()), 0.05)
	w.IObject.Drawing(imd, display)
}

//Update iUpdater
func (w *Wolf) Update() {
	w.sm.Reset()
	//e.sm.WanderRect(e.zone)
	//w.sm.Porsuit(w.target)
	//w.sm.Wander(0.7)

	w.sm.WanderRect(w.g.Grid.Rect())
	w.sm.CollisionAvoidance()
	//Идет к цели
	//dist := e.target.Copy().Sub(e.GetPosition()).Length()
	//e.sm.Seek(e.target, dist, 3.0)
	//Убегает от цели
	// mouseDist := e.g.Mouse.Copy().Sub(e.GetPosition()).Length()
	// e.sm.Away(e.g.Mouse.Copy(), mouseDist, 10.0)
	w.sm.Update()
}
