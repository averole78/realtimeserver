package core

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	melody "gopkg.in/olahol/melody.v1"
)

type Router struct {
	port    string
	ws      *melody.Melody
	e       *echo.Echo
	clients *ManagerClients
}

func NewRouter(port string, clients *ManagerClients) *Router {
	r := &Router{}
	r.port = port
	r.ws = melody.New()
	r.clients = clients
	//Запуск сервера
	r.e = echo.New()
	r.e.Use(middleware.Recover())
	r.e.Use(middleware.Logger())
	r.e.HideBanner = true
	r.ws.HandleConnect(func(s *melody.Session) {
		go clients.NewClient(s)
	})

	//Отключение
	r.ws.HandleDisconnect(func(s *melody.Session) {
		go clients.DeleteClient(s)
	})

	//Чтение данных
	r.ws.HandleMessageBinary(func(s *melody.Session, raw []byte) {
		go clients.ReadClient(s, raw)
	})

	r.e.GET("/cl/:id", func(c echo.Context) error {
		r.ws.HandleRequest(c.Response(), c.Request())
		return nil
	})
	return r
}

//Run старт сервера
func (r *Router) Run() {
	go r.e.Logger.Fatal(r.e.Start(r.port))
}
