package core

import (
	"realtimeserver/helpers/vector"
	"realtimeserver/protos"

	"github.com/faiface/pixel/imdraw"
	"golang.org/x/image/colornames"
)

type Unit struct {
	IObject
	Keyboards map[protos.KeyCode]*protos.Keyboard
	Mouse     map[protos.MouseCode]*protos.Mouse
	c         *Client
	direction *vector.V
}

func NewUnit(c *Client) *Unit {
	u := &Unit{}
	u.c = c
	u.Keyboards = make(map[protos.KeyCode]*protos.Keyboard)
	u.Mouse = make(map[protos.MouseCode]*protos.Mouse)
	u.IObject = NewObject(c.g.Grid, c.g.ObstacleGrid, 0.5, 80, 1, Speed)
	// u.SetMaxForce(1)
	// u.SetMaxSpeed(Speed)
	u.SetVelocity(&vector.Zero)
	u.direction = vector.UnitX.Copy()
	return u
}

//GetProto Возвращает прото объект
func (u *Unit) GetProto() *protos.Unit {
	pb := &protos.Unit{}
	pb.WID = uint32(u.GetID())
	pb.Position = u.GetPosition().ToProto()
	pb.Speed = u.GetVelocity().ToProto()
	return pb
}

//AddKeyBoard Добавляет в список нажатых клавищ
func (u *Unit) AddKeyBoard(kb *protos.Keyboard) {
	switch kb.Event {
	case protos.KeyEvent_ON_KEY_UP:
		//Снимаетм зажатие клавиши
		delete(u.Keyboards, kb.Code)
	case protos.KeyEvent_ON_KEY_DOWN:
		//Добавляем зажатие клавиши
		u.Keyboards[kb.Code] = kb
	case protos.KeyEvent_ON_KEY_PRESS:
		//Разовое зажатие клавиши
	}
}

//AddMouse Добавляет в список нажатых клавиш мыши
func (u *Unit) AddMouse(ms *protos.Mouse) {
	switch ms.Event {
	case protos.MouseEvent_ON_MOUSE_MOVE:
		u.direction = u.GetPosition().Copy().Add(vector.FromProto(ms.Coord).Normalize())
	case protos.MouseEvent_ON_MOUSE_UP:
		//Снимаетм зажатие клавиши
		delete(u.Mouse, ms.Code)
	case protos.MouseEvent_ON_MOUSE_DOWN:
		//Добавляем зажатие клавиши
		u.Mouse[ms.Code] = ms
	case protos.MouseEvent_ON_MOUSE_CLICK:
		//NewBot(ms.Coord, u.w)
		//Разовое зажатие клавиши
	case protos.MouseEvent_ON_MOUSE_DBLCLICK:
		//Разовое зажатие клавиши
	case protos.MouseEvent_ON_MOUSE_SCROLL_DOWN:
		//Разовое зажатие клавиши
	case protos.MouseEvent_ON_MOUSE_SCROLL_UP:
		//Разовое зажатие клавиши
	}
}

func (u *Unit) Update() {
	v := &vector.V{}
	for code, key := range u.Keyboards {
		if key.Event == protos.KeyEvent_ON_KEY_DOWN {
			if code == protos.KeyCode_W {
				v.AddY(u.GetMaxSpeed()) //вверх
			}
			if code == protos.KeyCode_S {
				v.AddY(-u.GetMaxSpeed()) //вниз
			}
			if code == protos.KeyCode_D {
				v.AddX(u.GetMaxSpeed())
			}
			if code == protos.KeyCode_A {
				v.AddX(-u.GetMaxSpeed())
			}
		}
	}
	u.SetVelocity(v)
	u.Move(u.GetVelocity())
}

func (u *Unit) Drawing(imd *imdraw.IMDraw, display *vector.Rect) {

	//body
	imd.Color = colornames.Yellow
	imd.Push(u.GetPosition().ToPixel())
	imd.Circle(u.GetRadius(), 0.05)

	// weapon
	imd.Color = colornames.Black
	imd.Push(u.GetPosition().ToPixel())
	imd.Push(u.direction.ToPixel())
	imd.Line(0.1)
}
