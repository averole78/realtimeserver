package core

import (
	"fmt"
	"io/ioutil"
	"os"
	"realtimeserver/helpers"
	"realtimeserver/helpers/vector"
	"time"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/pixelgl"
	"github.com/faiface/pixel/text"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/colornames"
	"golang.org/x/image/font"
)

type IDrawing interface {
	Drawing(imd *imdraw.IMDraw, display *vector.Rect)
}

type Display struct {
	Win          *pixelgl.Window
	MouseTarget  pixel.Vec
	atlas        *text.Atlas
	bar          *helpers.Chain
	g            *GameWorld
	cam          pixel.Matrix
	camPos       pixel.Vec
	camZoom      float64
	camZoomSpeed float64
	camSpeed     float64
	rect         *vector.Rect
}

func NewDisplay(g *GameWorld) *Display {
	d := &Display{}
	d.g = g
	d.bar = helpers.NewChain()

	return d
}

//Run DisplayRun
func (d *Display) Run() {

	cfg := pixelgl.WindowConfig{
		Title:  "Display RealTimeServer!",
		Bounds: pixel.R(0, 0, 800, 800),
		VSync:  true,
	}
	var err error
	d.Win, err = pixelgl.NewWindow(cfg)
	d.Win.SetSmooth(true)
	if err != nil {
		panic(err)
	}

	face, err := loadTTF("fonts/SourceSansPro-Black.ttf", 24)
	if err != nil {
		panic(err)
	}
	d.atlas = text.NewAtlas(face, text.ASCII)

	d.cam = pixel.IM
	d.camPos = pixel.ZV
	d.camZoom = 20
	d.camZoomSpeed = 1.0
	d.camSpeed = 50.0
	d.rect = vector.RectFromCircle(vector.Zero.Copy(), 0)

	last := time.Now()
	for !d.Win.Closed() {
		dt := time.Since(last).Seconds()
		last = time.Now()

		//cam := pixel.IM.Moved(d.Win.Bounds().Center().Sub(d.camPos))
		//cam := pixel.IM
		//d.Win.SetMatrix(cam)
		if d.Win.Pressed(pixelgl.KeyLeft) {
			d.camPos.X -= d.camSpeed * dt
		}
		if d.Win.Pressed(pixelgl.KeyRight) {
			d.camPos.X += d.camSpeed * dt
		}
		if d.Win.Pressed(pixelgl.KeyDown) {
			d.camPos.Y -= d.camSpeed * dt
		}
		if d.Win.Pressed(pixelgl.KeyUp) {
			d.camPos.Y += d.camSpeed * dt
		}

		//d.rect = vector.RectFromCircle(vector.NewVector(0, 0))
		//d.rect.
		//fmt.Println(d.Win.Canvas().Bounds())

		d.Win.Clear(colornames.Darkslategrey)
		d.Win.SetMatrix(pixel.IM.Scaled(d.camPos, d.camZoom).Moved(d.Win.Bounds().Center().Sub(d.camPos)))
		//d.camZoom *= math.Pow(d.camZoomSpeed, d.Win.MouseScroll().Y)
		p := d.Win.Bounds().Max

		//d.rect = pixel.Rect{Min: max.Scaled(-0.99), Max: max.Scaled(0.99)}

		max := vector.NewVector(p.X/d.camZoom/2, p.Y/d.camZoom/2)
		d.rect = &vector.Rect{max.Copy().Scale(-1.1), max.Scale(1.1)}
		d.rect = d.rect.Move(vector.NewVector(d.camPos.X, d.camPos.Y))

		d.DisplayRedraw()

		d.WriteBar()
		d.MouseUpdate()
		d.AddBar("frame", fmt.Sprintf("%d", d.g.frame))
		d.AddBar("objs", fmt.Sprintf("%d", d.g.Updates.Len()))
		d.AddBar("DeltaTime", fmt.Sprintf("%d", d.g.DeltaTime))
		d.AddBar("countUpdateGrid", fmt.Sprintf("%d", countUpdateGrid))
		d.Win.Update()
	}
}

//MouseUpdate MouseUpdate
func (d *Display) MouseUpdate() {
	t := d.Win.MousePosition()
	t.X = d.camPos.X - (t.X / d.camZoom)
	t.Y = d.camPos.Y - (t.Y / d.camZoom)
	d.MouseTarget = t
	d.g.UpdateMouse(vector.NewVector(t.X, t.Y))
	d.AddBar("mouse", fmt.Sprintf("%s", d.MouseTarget))
}

func (d *Display) AddBar(key, str string) {
	if d.bar == nil {
		return
	}
	d.bar.Set(key, str)
}

//WriteBar WriteBar
func (d *Display) WriteBar() {
	if d.atlas == nil {
		return
	}
	txt := text.New(pixel.V(0, 0), d.atlas)
	txt.Color = colornames.Greenyellow
	d.bar.Range(func(k, v interface{}) bool {
		fmt.Fprintf(txt, "%s: %s\n", k.(string), v.(string))
		return true
	})
	txt.Draw(d.Win, pixel.IM.Scaled(txt.Orig, 0.03))
}

//DisplayRedraw Отрисовка дисплея
func (d *Display) DisplayRedraw() {
	imd := imdraw.New(nil)
	d.g.Drawing(imd, d.rect)

	d.redDote(imd) //center cross
	imd.Draw(d.Win)
}

func (d *Display) redDote(imd *imdraw.IMDraw) {
	//camera
	imd.Color = colornames.Yellow
	vs := d.rect.Vertices()
	imd.Push(vs[0].ToPixel(), vs[1].ToPixel(), vs[2].ToPixel(), vs[3].ToPixel())
	imd.Line(0.05)
	//red cross
	imd.Color = colornames.Black
	imd.Push(pixel.V(0, 0.5), pixel.V(0, -0.5))
	imd.Line(0.05)
	imd.Push(pixel.V(0.5, 0), pixel.V(-0.5, 0))
	imd.Line(0.05)
}

func loadTTF(path string, size float64) (font.Face, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	font, err := truetype.Parse(bytes)
	if err != nil {
		return nil, err
	}

	return truetype.NewFace(font, &truetype.Options{
		Size:              size,
		GlyphCacheEntries: 1,
	}), nil
}
