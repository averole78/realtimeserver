package core

import (
	"math/rand"
	"realtimeserver/helpers/vector"

	"realtimeserver/protos"

	"github.com/faiface/pixel/imdraw"
	"golang.org/x/image/colornames"
)

//Obstacle Препятствие
type Obstacle struct {
	IObject
}

//Serialize Возвращает прото объект
func (ob *Obstacle) Serialize() *protos.Obstacle {
	t := &protos.Obstacle{}
	t.ID = uint32(ob.GetID())
	t.Position = ob.GetPosition().ToProto()
	t.Radius = float32(ob.GetRadius())
	return t
}

//NewObstacle NewObstacle
func NewObstacle(g *GameWorld) *Obstacle {
	e := &Obstacle{}
	e.IObject = NewObject(g.Grid, g.ObstacleGrid, rand.Float64()*3+1, 0, 0, 0)
	return e
}

//Drawing Рисовать
func (ob *Obstacle) Drawing(imd *imdraw.IMDraw, display *vector.Rect) {
	//	Circle
	imd.Color = colornames.Black
	imd.Push(ob.GetPosition().ToPixel())
	imd.Circle(ob.GetRadius(), 0)

	v := vector.Unit45.Copy().Scale(ob.GetRadius() * 0.8)
	imd.Color = colornames.Gray
	imd.Push(ob.GetPosition().Copy().Add(v).ToPixel())
	imd.Push(ob.GetPosition().Copy().Add(v.Invert()).ToPixel())
	imd.Line(0.1 * ob.GetRadius())
	imd.Push(ob.GetPosition().Copy().Add(v.Rotate90DegRight()).ToPixel())
	imd.Push(ob.GetPosition().Copy().Add(v.Invert()).ToPixel())
	imd.Line(0.1 * ob.GetRadius())
}

//Update iUpdater
func (ob *Obstacle) Update() {

}
