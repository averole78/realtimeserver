package core

import (
	"realtimeserver/helpers"
	"realtimeserver/helpers/vector"
)

type ICollision interface {
	IObject2D
	Collision(imp Impulse)
}

//Layer слои столкновений
type Layer int

//Collider Collider
type Collider struct {
	layers []*helpers.Grid
}

//NewCollider NewCollider
func NewCollider() *Collider {
	c := &Collider{}
	c.layers = make([]*helpers.Grid, 0, 0)
	return c
}

//NewLayer новый слой столкновений
//grid Сетка расположений объктов
//themselves учитывает или нет столкновения между собой
func (c *Collider) NewLayer(grid *helpers.Grid) Layer {
	c.layers = append(c.layers, grid)
	return Layer(len(c.layers) - 1)
}

//Set Добавляет в колайдер
func (c *Collider) Set(obj ICollision, layer Layer) {
	c.layers[layer].Set(int(obj.Position().GetX()), int(obj.Position().GetY()), obj)
}

//Del Удаляет из коллайдера
func (c *Collider) Del(obj ICollision, layer Layer) {
	c.layers[layer].Del(int(obj.Position().GetX()), int(obj.Position().GetY()), obj)
}

//SearchCollision Находим все столкновения объекта
func (c *Collider) SearchCollision(obj ICollision, exclude ...Layer) []*Impulse2D {
	resp := make([]*Impulse2D, 0, 0)
	//Перебираем слои
	for l, grid := range c.layers {
		//Исключаем слои
		for _, ex := range exclude {
			if int(ex) == l {
				continue
			}
		}
		//Проверяем 4 соседних клетки
		v4 := CalcRose(obj, grid)
		for _, v := range v4 {
			//Перебираем цепь
			grid.Range(int(v.GetX()), int(v.GetY()), func(p helpers.IElement) bool {
				//Себя не учитываем
				if obj.GetID() == p.(ICollision).GetID() {
					return true
				}
				//Расчитываем дистанцию между обьектами
				dist := v.Copy().Sub(p.(ICollision).Position())
				if dist.Length() < (obj.Radius() + p.(ICollision).Radius()) {
					imp := &Impulse2D{}
					imp.Velocity = dist.Copy().Normalize().Scale(obj.Radius() + p.(ICollision).Radius())
					imp.Obj = p.(ICollision)
					resp = append(resp, imp)
				}
				return true
			})
		}

	}
	return resp
}

//CalcRose Расчитывет проверочные ячейки в сетке
func CalcRose(obj ICollision, grid *helpers.Grid) [4]*vector.V {
	v4 := [4]*vector.V{}
	cntr := obj.Position().Copy().RoundRose(float64(grid.SizeCell()))
	q45 := vector.Unit45.Copy()
	v4[0] = cntr.Copy().Add(q45)
	v4[1] = cntr.Copy().Add(q45.Rotate90DegLeft())
	v4[2] = cntr.Copy().Add(q45.Rotate90DegLeft())
	v4[3] = cntr.Copy().Add(q45.Rotate90DegLeft())
	return v4
}

//Impulse2D Объкт импульс от столкновения
type Impulse2D struct {
	Obj      ICollision
	Velocity *vector.V
}
