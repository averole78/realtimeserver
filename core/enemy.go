package core

import (
	"math/rand"
	"realtimeserver/helpers/vector"

	"realtimeserver/protos"

	"github.com/faiface/pixel/imdraw"
)

//Enemy Вражеский юнит
type Enemy struct {
	IObject
	sm     *SteeringManager
	zone   *vector.Rect
	target IObject
	path   *vector.Path
}

//Serialize Возвращает прото объект
func (e *Enemy) Serialize() *protos.Enemy {
	pb := &protos.Enemy{}
	pb.ID = uint32(e.GetID())
	pb.Position = e.GetPosition().ToProto()
	pb.Speed = e.GetVelocity().ToProto()
	return pb
}

//NewEnemy NewEnemy
func NewEnemy(g *GameWorld) *Enemy {
	e := &Enemy{}
	e.IObject = NewObject(g.Grid, g.ObstacleGrid, 0.5, 100, 1, 0.01)

	e.zone = vector.RectFromCircle(vector.Zero.Copy().AddXY(10), 10)

	e.SetVelocity(vector.UnitX.Copy().Truncate(Speed))
	//	e.SetRadius(0.5)
	e.sm = NewSteeringManager(e)
	e.target = e
	e.path = e.GenPath(e.zone, 2)
	e.sm.SetPathForFollowing(e.path)
	return e
}

//RandTarget Выбирает случайную цель
func (e *Enemy) RandTarget() *vector.V {

	return vector.NewVector(rand.Float64()*40-20, rand.Float64()*40-20)
}

//SetTarget Выбирает случайную цель
func (e *Enemy) SetTarget(t IObject) {
	e.target = t
}

//Drawing Рисовать
func (e *Enemy) Drawing(imd *imdraw.IMDraw, display *vector.Rect) {
	//Zone
	// imd.Color = colornames.Gray
	// imd.Push(e.zone.Min().ToPixel(), e.zone.Max().ToPixel())
	// imd.Rectangle(0.05)

	//	path
	// imd.Color = colornames.Blue
	// for index := 0; index < e.path.Len(); index++ {
	// 	imd.Push(e.path.GetPoint(index).ToPixel())
	// }
	// imd.Circle(0.5, 0.05)
	e.IObject.Drawing(imd, display)
}

func (e *Enemy) GenPath(r *vector.Rect, count int) *vector.Path {
	path := vector.NewPath()
	for index := 0; index < count; index++ {
		v := e.zone.RandPoint()
		v[0] = r.Min()[0] + (r.W()/float64(count))*float64(index)

		path.AddPoint(v)
	}
	return path
}

//Update iUpdater
func (e *Enemy) Update() {
	return
	e.sm.Reset()
	//e.sm.WanderRect(e.zone)
	//e.sm.Wander(0.3)
	//e.sm.Seek(vector.Zero.Copy(), 5)
	//e.sm.Evade(e.target)
	// if e.sm.PathFollowing(1) {
	// 	e.path = e.GenPath(e.zone, 2)
	// 	e.sm.SetPathForFollowing(e.path)
	// }
	//e.sm.Wander(0.25)
	//Идет к цели
	//dist := e.target.Copy().Sub(e.GetPosition()).Length()
	//e.sm.Seek(e.target, dist, 3.0)
	//Убегает от цели
	// mouseDist := e.g.Mouse.Copy().Sub(e.GetPosition()).Length()
	// e.sm.Away(e.g.Mouse.Copy(), mouseDist, 10.0)
	e.sm.Update()

}
