package core

import (
	"fmt"
	"realtimeserver/helpers"
	"realtimeserver/helpers/vector"
	"time"

	"github.com/faiface/pixel/imdraw"
)

const Speed = 0.02314816666

//GameWorld GameWorld
type GameWorld struct {
	frame        uint
	frameRate    uint
	Updates      *helpers.Chain //Обьекты обновления
	Drawers      *helpers.Chain
	Mouse        *vector.V
	Grid         *helpers.Grid
	ObstacleGrid *helpers.Grid
	DeltaTime    time.Duration
	Collider     *Collider
}

func NewWorld() *GameWorld {
	g := &GameWorld{}
	g.frameRate = 60
	g.frame = 0
	g.Grid = helpers.NewGrid(10, 4)
	g.ObstacleGrid = helpers.NewGrid(4, 10)
	g.Drawers = helpers.NewChain()
	g.Updates = helpers.NewChain()
	g.Mouse = vector.Zero.Copy()
	g.Collider = NewCollider()
	//Инициализация пространства
	//g.Space = NewSpaceMap(5, 1)

	//таймер добавляет ботов
	// tickerAdd := time.NewTicker(500 * time.Millisecond)
	// go func() {
	// 	for range tickerAdd.C {
	// 		if g.Updates.Len() >= 1 {
	// 			return
	// 		}
	// 		en := NewEnemy(g)
	// 		g.AddMovedObject(en)
	// 	}
	// }()
	//Obstacle
	for index := 0; index < 5; index++ {
		ob := NewObstacle(g)
		g.AddDrawing(ob)
	}

	//enemys
	// for index := 0; index < 5; index++ {
	// 	en := NewEnemy(g)
	// 	g.AddUpdater(en)
	// 	g.AddDrawing(en)
	// }
	//wolfs
	for index := 0; index < 10; index++ {
		w := NewWolf(g)
		g.AddUpdater(w)
		g.AddDrawing(w)
	}

	return g
}

// Drawing Отрисовка
func (g *GameWorld) Drawing(imd *imdraw.IMDraw, display *vector.Rect) {
	g.Drawers.Range(func(key, value interface{}) bool {
		key.(IDrawing).Drawing(imd, display)
		return true
	})
}

func (g *GameWorld) Update() {
	g.frame++
	g.Updates.Range(func(key, value interface{}) bool {
		key.(IUpdater).Update()
		return true
	})
}

//Run Старт сцены
func (g *GameWorld) Run() {
	PerSecond := uint(time.Second) / g.frameRate
	var start, now time.Time
	var delta time.Duration
	for {
		start = time.Now()
		g.Update()
		now = time.Now()
		delta = time.Duration(PerSecond) - now.Sub(start)
		if delta <= 0 {
			fmt.Printf("!!!delta<0:%+v\n", delta)
			continue
		}
		g.DeltaTime = delta
		time.Sleep(delta)
	}
}

//AddDrawing Добавляет юнита в игру
func (g *GameWorld) AddDrawing(u IDrawing) {
	g.Drawers.Set(u, struct{}{})
}

// DelDrawing Удаляет юнита из игры
func (g *GameWorld) DelDrawing(u IDrawing) {
	g.Drawers.Del(u)
}

//AddUpdater Добавляет юнита в игру
func (g *GameWorld) AddUpdater(u IUpdater) {
	g.Updates.Set(u, struct{}{})
}

// DelUpdater Удаляет юнита из игры
func (g *GameWorld) DelUpdater(u IUpdater) {
	g.Updates.Del(u)
}

// //AddCollider Добавляет юнита в игру
// func (g *GameWorld) AddCollider(u ICollision, layer Layer) {
// 	g.Collider.Set(u, layer)
// }

// // DelCollider Удаляет юнита из игры
// func (g *GameWorld) DelCollider(u ICollision, layer Layer) {
// 	g.Collider.Del(u, layer)
// }

func (g *GameWorld) UpdateMouse(v *vector.V) {
	g.Mouse = v
}
