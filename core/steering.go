package core

import (
	"math"
	"math/rand"
	"realtimeserver/helpers/vector"
)

/*
https://gamedevelopment.tutsplus.com/tutorials/understanding-steering-behaviors-movement-manager--gamedev-4278
*/
const MAX_SEE_AHEAD = 4
const MAX_AVOID_FORCE = 0.2

//SteeringManager Управление рулевым колесом движущегося объекта
type SteeringManager struct {
	obj               IObject
	framesToNewTarget int32
	wanderAngle       float64
	curPath           int
	path              *vector.Path
	steering          *vector.V
	target            *vector.V
	ahead             *vector.V
	avoidanceForce    *vector.V
	aheadRad          float64
}

//NewSteeringManager constructor
func NewSteeringManager(obj IObject) *SteeringManager {
	sm := &SteeringManager{}
	sm.obj = obj
	sm.steering = vector.Zero.Copy()
	sm.framesToNewTarget = 0
	sm.target = obj.GetPosition()
	sm.wanderAngle = rand.Float64() * (math.Pi * 2)
	sm.ahead = vector.Zero.Copy()
	sm.avoidanceForce = vector.Zero.Copy()

	return sm
}

//Reset Сбросить внутреннюю силу рулевого управления
func (sm *SteeringManager) Reset() {
	sm.steering.Scale(0)
}

//Update Должен быть вызван после того, как все поведения были вызваны
func (sm *SteeringManager) Update() {
	//ограничение силы
	sm.steering.Truncate(sm.obj.GetMaxForce()).Scale(1 / sm.obj.GetMass())

	//ограничение скорости
	vel := sm.obj.GetVelocity().Add(sm.steering).Truncate(sm.obj.GetMaxSpeed())
	sm.obj.Move(vel)
}

//CollisionAvoidance избегание коллизий
func (sm *SteeringManager) CollisionAvoidance() {

	//Убираем пересечение
	imp := sm.obj.GetImpulse()
	if imp != nil {
		//Отскакивает перепендикулярно столкновению
		perp := sm.obj.GetPosition().Copy().Sub(imp.Obj.GetPosition()).Normalize().Scale(MAX_AVOID_FORCE)
		sm.steering.Add(perp)
		return
	}

	dynamic_length := sm.obj.GetVelocity().Length() / sm.obj.GetMaxSpeed()
	//Проверяем ближний луч
	sm.ahead = sm.obj.GetPosition().Copy().Add(sm.obj.GetVelocity().Copy().Normalize().Scale(dynamic_length * MAX_SEE_AHEAD * 0.3))
	imp = sm.obj.Collision(sm.ahead, sm.ahead, sm.obj.GetRadius()*1.3)
	sm.aheadRad = sm.obj.GetRadius() * 1.3
	if imp == nil {
		//Проверяем дальний луч
		sm.ahead.Add(sm.obj.GetVelocity().Copy().Normalize().Scale(dynamic_length * MAX_SEE_AHEAD * 0.7))
		imp = sm.obj.Collision(sm.ahead, sm.ahead, sm.obj.GetRadius()*1.7)
		sm.aheadRad = sm.obj.GetRadius() * 1.7
		if imp == nil {
			sm.avoidanceForce.Scale(0)
			return
		}
	}
	sm.avoidanceForce = sm.ahead.Copy().Sub(imp.Obj.GetPosition()).Normalize().Scale(MAX_AVOID_FORCE)
	sm.steering.Add(sm.avoidanceForce)
}

//Seek Поиск объекта
// Возвращает дистанцию до обьекта
func (sm *SteeringManager) Seek(target *vector.V, slowingRadius float64) float64 {
	dist := target.Copy().Sub(sm.obj.GetPosition()).Length()
	//desired velocity
	dv := target.Copy().Sub(sm.obj.GetPosition()).Normalize().Scale(sm.obj.GetMaxSpeed())
	if dist < slowingRadius {
		dv.Scale((dist / 2) / slowingRadius)
	}
	//temp steering
	st := dv.Copy().Sub(sm.obj.GetVelocity())
	sm.steering.Add(st)
	return dist
}

//Away Убегание прочь то цели
//Если цели ближе радиуса
func (sm *SteeringManager) Away(target *vector.V, dist, radius float64) {
	if dist > radius {
		return
	}
	//desired velocity
	dv := target.Copy().Sub(sm.obj.GetPosition()).Normalize().Scale(sm.obj.GetMaxSpeed()).Invert()
	//temp steering
	st := dv.Copy().Sub(sm.obj.GetVelocity())
	sm.steering.Add(st)
}

//WanderRect Бродить по прямоугольной области
func (sm *SteeringManager) WanderRect(r *vector.Rect) {
	if sm.framesToNewTarget <= 0 {
		sm.target = r.RandPoint()
		sm.framesToNewTarget = rand.Int31n(300) + 300
	}
	sm.framesToNewTarget--
	//Проверка расстояния до цели
	dist := sm.Seek(sm.target, 2)
	if dist < sm.obj.GetRadius() {
		sm.target = r.RandPoint()
	}
}

//Wander Бродить
//anglechange от 0 до 1
func (sm *SteeringManager) Wander(anglechange float64) {
	sm.wanderAngle += (rand.Float64() * anglechange) - (anglechange * 0.5)
	wanderForce := sm.obj.GetVelocity().Copy().Normalize().Add(vector.UnitY.Copy().Rotate(sm.wanderAngle))
	sm.steering.Add(wanderForce)
}

//Porsuit Догоняет другой обьект
func (sm *SteeringManager) Porsuit(t IObject) {
	dist := t.GetPosition().Copy().Sub(sm.obj.GetPosition())
	T := dist.Length() / sm.obj.GetMaxSpeed()
	fPos := t.GetPosition().Copy().Add(t.GetVelocity().Copy().Scale(T))
	sm.Seek(fPos, 1)
}

//Evade Убегает от объекта
func (sm *SteeringManager) Evade(t IObject) {
	dist := t.GetPosition().Copy().Sub(sm.obj.GetPosition())
	T := dist.Length() / sm.obj.GetMaxSpeed()
	fPos := t.GetPosition().Copy().Add(t.GetVelocity().Copy().Scale(T))
	sm.Away(fPos, 1, 5)
}

//SetPathForFollowing SetPathForFollowing
func (sm *SteeringManager) SetPathForFollowing(p *vector.Path) {
	sm.path = p
	sm.curPath = 0
}

//PathFollowing Следует до конца пути
//Возвращает true, если достиг конца пути
func (sm *SteeringManager) PathFollowing(smooth float64) bool {
	if sm.path == nil {
		return true
	}
	if sm.curPath >= sm.path.Len() {
		return true
	}
	t := sm.path.GetPoint(sm.curPath)
	//Перемещение по пути
	dist := sm.obj.GetPosition().Copy().Sub(t).Length()
	//Пересекаем точку
	if dist < smooth {
		sm.curPath++
	}
	sm.Seek(t, smooth)
	return false
}
