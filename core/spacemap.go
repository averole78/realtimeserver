package core

import (
	"fmt"
	"realtimeserver/helpers/vector"
	"sync"
)

type Box [4]*Point

//Point Точка в пространстве
type Point struct {
	prev *Point
	next *Point
	v    *vector.V   //Координаты точки
	h    vector.Hash //Хеш сетки
	obj  IObject     //Принадлежность объекту
}

//NewPoint новая точка в простанстве
func NewPoint(v *vector.V, obj IObject) *Point {
	return &Point{v: v, h: v.ToHash(), obj: obj}
}
func (p *Point) GetObject() IObject {
	return p.obj
}

//===================================================================

//Galaxy Множество точек
type Galaxy struct {
	first *Point
	count int
	mu    *sync.Mutex
}

//NewGalaxy Новое созвездие
func NewGalaxy() *Galaxy {
	return &Galaxy{mu: &sync.Mutex{}}
}

//Range Проходим по всем точкам созвездия
func (g *Galaxy) Range(f func(p *Point) bool) {
	g.mu.Lock()
	defer g.mu.Unlock()
	if g.first == nil {
		return
	}
	p := g.first
	for {
		if f(p) == false {
			return
		}
		if p.next == nil {
			return
		}
		p = p.next
	}
}

//Add Добавляет точку
func (g *Galaxy) Add(p *Point) {
	g.mu.Lock()
	if g.first != nil {
		g.first.prev = p
	}
	p.next = g.first
	g.first = p
	g.count++
	g.mu.Unlock()
}

//Del Удаляет точку
func (g *Galaxy) Del(p *Point) {
	g.mu.Lock()
	//Удаляем единственную точку
	if g.count == 1 {
		g.first = nil
	}
	//Удаляем из середины созвездия
	if p.prev != nil && p.next != nil {
		p.prev.next = p.next
		p.next.prev = p.prev
	}
	//Удаляем в начале созвездия
	if p.prev == nil && p.next != nil {
		g.first = p.next
		p.next.prev = nil
	}
	//Удаляем в конце созвездия
	if p.next == nil && p.prev != nil {
		p.prev.next = nil
	}
	p.next = nil
	p.prev = nil
	g.count--
	g.mu.Unlock()
}

//===================================================================

//SpaceMap пространство объектов
type SpaceMap struct {
	countID      uint64
	step, length int32
	mu           *sync.Mutex
	galaxys      map[vector.Hash]*Galaxy
	obj          map[IObject]*Object
	rect         *vector.Rect
}

//NewSpaceMap новое пространство объектов
//maxlength максимальная положительная длина пространства
//step шаг сетки
func NewSpaceMap(maxlength, step int32) *SpaceMap {
	sm := &SpaceMap{}
	sm.mu = &sync.Mutex{}
	sm.step = step
	sm.length = maxlength
	sm.step = step
	sm.galaxys = make(map[vector.Hash]*Galaxy)
	sm.obj = make(map[IObject]*Object)
	sm.rect = vector.RectFromCircle(vector.Zero.Copy(), float64(maxlength))
	fmt.Println(sm.rect)
	return sm
}

//Clamp обрезает вектор до границ
func (sm *SpaceMap) Clamp(v *vector.V) {
	v.Clamp(sm.rect.Min(), sm.rect.Max())
}

//=======================================================
