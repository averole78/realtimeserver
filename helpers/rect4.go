package helpers

import (
	"math"

	"github.com/faiface/pixel"
)

//Rect4 состоит из 4 прямоугольников(квадратов)
//В зависимости от расположения точки в среднем прямоугольнике
//Создаются еще 3 прямоугольника таким образом чтобы быть ближе к точке
const (
	LD = iota //Слева внизу
	LU        //Слева вверху
	RU        //Справа вверху
	RD        //Справа внизу
)

type Rect4 struct {
	r4   [4]pixel.Rect
	size float64    //размер стороны прямоугольника
	sr   pixel.Rect //малый прямоугольник
	n    int        //в каком углу среднего прямоуголника находится малый
}

//NewReсt4 Создание нового Rect4
func NewReсt4(size float64, v pixel.Vec) *Rect4 {
	r := &Rect4{}
	r.size = size
	r.SetPoint(v)
	return r
}

//GetMyRect Возвращает средний прямоугольник в плоскости которого лежит вектор
func (r *Rect4) GetMyRect() pixel.Rect {
	return r.r4[0]
}

//GetRect4 Возвращает массив из 4 прямоугольников
func (r *Rect4) GetRect4() [4]pixel.Rect {
	return r.r4
}

//GetCoord GetCoordinate(v)
func (r *Rect4) GetCoord(v pixel.Vec) Coord {
	return Coord(int64(v.X)<<32 + int64(v.Y))
}

//GetMyCoord coord[0]
func (r *Rect4) GetMyCoord() Coord {
	return Coord(int64(r.r4[0].Min.X)<<32 + int64(r.r4[0].Min.Y))
}

//SetPoint Устанавливает точку расчета 4 прямоугольников
func (r *Rect4) SetPoint(v pixel.Vec) Coord {
	//Если v лежит в плоскоcти малого прямоугольника
	//ничего неменяем
	if r.sr.Contains(v) {
		return r.GetMyCoord()
	}
	//Расчет среднего прямоугольника
	r.updateMediumRect(v)
	//Расчет малого прямоугольника
	r.updateSmallRect(v, r.r4[0])
	//Расчет 4 прямоугольников
	r.update4Rect()
	return r.GetMyCoord()
}

//get4Rect Нахождение среднего прямоугольника
func (r *Rect4) update4Rect() {
	switch r.n {
	case LD:
		r.r4[1] = r.r4[0].Moved(pixel.V(0, -r.size))
		r.r4[2] = r.r4[0].Moved(pixel.V(-r.size, -r.size))
		r.r4[3] = r.r4[0].Moved(pixel.V(-r.size, 0))
	case LU:
		r.r4[1] = r.r4[0].Moved(pixel.V(-r.size, 0))
		r.r4[2] = r.r4[0].Moved(pixel.V(-r.size, r.size))
		r.r4[3] = r.r4[0].Moved(pixel.V(0, r.size))
	case RU:
		r.r4[1] = r.r4[0].Moved(pixel.V(0, r.size))
		r.r4[2] = r.r4[0].Moved(pixel.V(r.size, r.size))
		r.r4[3] = r.r4[0].Moved(pixel.V(r.size, 0))
	case RD:
		r.r4[1] = r.r4[0].Moved(pixel.V(r.size, 0))
		r.r4[2] = r.r4[0].Moved(pixel.V(r.size, -r.size))
		r.r4[3] = r.r4[0].Moved(pixel.V(0, -r.size))
	}
}

//getRect Нахождение среднего прямоугольника
func (r *Rect4) updateMediumRect(v pixel.Vec) {
	v.X = math.Floor(v.X/r.size) * r.size
	v.Y = math.Floor(v.Y/r.size) * r.size
	r.r4[0] = pixel.R(v.X, v.Y, v.X+r.size, v.Y+r.size).Norm()
}

//getRect Нахождение малого прямоугольника в среднем
func (r *Rect4) updateSmallRect(v pixel.Vec, mr pixel.Rect) {
	////Слева внизу по умолчанию
	r.n = LD
	r.sr.Min = mr.Min
	r.sr.Max = mr.Center()
	t := v.Sub(r.sr.Max)
	if t.X < 0 && t.Y > 0 { //Слева вверху
		r.sr = r.sr.Moved(pixel.V(0, r.size/2))
		r.n = LU

	} else if t.X > 0 && t.Y > 0 { //Справа вверху
		r.sr = r.sr.Moved(pixel.V(r.size/2, r.size/2))
		r.n = RU

	} else if t.X > 0 && t.Y < 0 { //Справа внизу
		r.sr = r.sr.Moved(pixel.V(r.size/2, 0))
		r.n = RD
	}
}

