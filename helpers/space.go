package helpers

import (
	"realtimeserver/helpers/vector"
	"sync"
)

//Space представляет из  себя многомерный массив(квадрат)
//с длиной сторон
//и размером ячейки step
//Вектор 0,0 лежит в центре квадрата
type Space struct {
	slice                     [][]interface{}
	mu                        *sync.RWMutex
	center, full, lengt, step int64
}

//NewSpace Новый
func NewSpace(lengt, step int16) *Space {
	s := &Space{}
	s.step = int64(step)
	s.lengt = int64(lengt)
	s.full = s.step * s.lengt
	s.center = s.full / 2
	s.slice = make([][]interface{}, lengt, lengt)
	for index := 0; index < int(lengt); index++ {
		s.slice[index] = make([]interface{}, lengt, lengt)
	}
	s.mu = &sync.RWMutex{}
	return s
}

func (s *Space) Set(v *vector.V, data interface{}) {
	x := s.convert(v.GetX())
	y := s.convert(v.GetY())
	s.mu.Lock()
	s.slice[x][y] = data
	s.mu.Unlock()
}

func (s *Space) Get(v *vector.V) interface{} {
	x := s.convert(v.GetX())
	y := s.convert(v.GetY())
	s.mu.RLock()
	r := s.slice[x][y]
	s.mu.RUnlock()
	return r
}

func (s *Space) convert(f float64) int64 {

	return ((s.full + s.center + (int64(f) % s.full)) % s.full) / s.step
}

func (s *Space) Convert(v *vector.V) *vector.V {
	return &vector.V{
		float64(s.convert(v.GetX())),
		float64(s.convert(v.GetY())),
	}
}

// func (s *Space) Hash(v *vector.V) int64 {
// 	return vector.NewVector(float64(s.convert(v.GetX())), float64(s.convert(v.GetY()))).ToInt64()
// }


//Range Проходим по всем областям
func (s *Space) Range(f func(v *vector.V) bool) {
	for j := 0; j < int(s.lengt); j++ {
		for i := 0; i < int(s.lengt); i++ {
			if f(vector.NewVector(float64(j), float64(i)).Scale(float64(s.step))) == false {
				return
			}
		}
	}
}
