package helpers

import (
	"fmt"
	"testing"
)

func TestTrain(t *testing.T) {
	a := make([]*Carriage, 0, 10)
	l := NewLocomotive()
	//Add
	for index := 0; index < 10; index++ {
		w := NewCarriage(index)
		a = append(a, w)
		w.Connect(l)
	}
	if l.Len() != len(a) {
		t.Error("Не добавился вагон ")
	}
	//Del
	a[0].Disconnect()
	a[5].Disconnect()
	a[9].Disconnect()
	if l.Len() != len(a)-3 {
		t.Error("Не удалился вагон ")
	}
	//Range
	fmt.Println(l)
}
