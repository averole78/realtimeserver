package helpers

import (
	"fmt"
	"math/rand"
	"realtimeserver/helpers/vector"
)

//Grid Сетка
type Grid struct {
	size  int
	cell  int
	slice []IElement
	rect  *vector.Rect
}

func NewGrid(size, cellsize uint16) *Grid {
	g := &Grid{}
	g.size = int(size)
	g.cell = int(cellsize)
	g.slice = make([]IElement, g.size*g.size, g.size*g.size)
	g.rect = &vector.Rect{vector.NewVector(0, 0), vector.NewVector(float64(g.size*g.cell), float64(g.size*g.cell))}
	return g
}

func (g *Grid) Set(a, b int, p IElement) error {
	if !g.Check(a, b) {
		return fmt.Errorf("Выход за пределы сетки a=%d, b=%d", a, b)
	}
	a /= g.cell
	b /= g.cell
	a = (a * g.size) + b
	if g.slice[a] == nil {
		g.slice[a] = p
	} else {
		p.SetNext(g.slice[a])
		g.slice[a] = p
	}
	return nil
}

func (g *Grid) Size() int {
	return g.size
}
func (g *Grid) SizeCell() int {
	return g.cell
}
func (g *Grid) Get(a, b int) IElement {
	if !g.Check(a, b) {
		return nil
	}
	a /= g.cell
	b /= g.cell
	return g.slice[(a*g.size)+b]
}

func (g *Grid) Del(a, b int, p IElement) {
	if !g.Check(a, b) {
		return
	}
	a /= g.cell
	b /= g.cell
	a = (a * g.size) + b

	if g.slice[a] == nil {
		return
	}
	//first element
	t := g.slice[a]
	if t == p {
		g.slice[a] = p.GetNext()
		p.SetNext(nil)
		return
	}
	//next
	for {
		if t.GetNext() == p {
			t.SetNext(p.GetNext())
			p.SetNext(nil)
			return
		}
		t = t.GetNext()
		if t == nil {
			return
		}
	}
}
func (g *Grid) Rect() *vector.Rect {
	return g.rect
}
func (g *Grid) Rand() *vector.V {
	return vector.NewVector(float64(rand.Intn(g.Size()*g.cell)), float64(rand.Intn(g.Size()*g.cell)))
}
func (g *Grid) Center() *vector.V {
	return vector.NewVector(float64(g.size/2), float64(g.size/2))
}
func (g *Grid) Hash(a, b int) vector.Hash {
	return vector.NewVector(float64(a/g.cell), float64(a/g.cell)).ToHash()
}
func (g *Grid) Check(a, b int) bool {
	if a < 0 || a >= g.size*g.cell || b < 0 || b >= g.size*g.cell {
		return false
	}
	return true
}

func (g *Grid) Range(a, b int, f func(p IElement) bool) {
	if !g.Check(a, b) {
		return
	}
	a /= g.cell
	b /= g.cell
	a = (a * g.size) + b
	t := g.slice[a]
	if t == nil {
		return
	}
	for {
		if !f(t) {
			return
		}
		t = t.GetNext()
		if t == nil {
			return
		}
	}
}

//IElement Элемент из сетки
type IElement interface {
	GetNext() IElement
	SetNext(p IElement)
}

// //Particle Частица сетки
// type Particle struct {
// 	next *Particle
// 	data interface{}
// }

// func NewParticle(data interface{}) *Particle {
// 	return &Particle{data: data}
// }

// func (prt *Particle) GetData() interface{} {
// 	return prt.data
// }

// //GetNext GetNext particle or nil
// func (prt *Particle) GetNext() *Particle {
// 	return prt.next
// }

// //SetNext SetNext particle
// func (prt *Particle) SetNext(p *Particle) {
// 	prt.next = p
// }
