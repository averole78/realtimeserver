package helpers

import (
	"realtimeserver/helpers/vector"
)

type Coord int64
type void struct{}

//MultiList плоскость состоящая из блоков прямоугольной формы содержащая в себе объекты
type MultiList struct {
	block *Chain
	size  float64
}

//Square Выдает до 4 точек соприкосновения с разными плоскостями
// на основе положения и радиуса
func Square(center *vector.V, r float64) map[Coord]*vector.V {
	//d = 2r√2  Формула диагонали квадрата через радиус вписанной окружности:
	m := make(map[Coord]*vector.V)
	//math.Sqrt(2)
	// d := r * math.Sqrt2 //половина диагонали
	// vector.UnitXY.Normalize()
	return m
}

//NewMultiList Создает новый объект MultiList
func NewMultiList(size float64) *MultiList {
	return &MultiList{block: NewChain(), size: size}
}

//Add Добавляет объект в ячейку с координатами
func (p *MultiList) Add(v Coord, obj interface{}) {
	tch, ok := p.block.Get(v)
	var ch *Chain
	if ok == false {
		ch = p.newBlock(v)
	} else {
		ch = tch.(*Chain)
	}
	ch.Set(obj, void{})
}

//Remove Удаляет обькт из ячейки
func (p *MultiList) Remove(v Coord, obj interface{}) {
	block, ok := p.block.Get(v)
	if ok == false {
		return
	}
	block.(*Chain).Del(obj)
}

//Range перебирает объекты из ячейки
func (p *MultiList) Range(v Coord, f func(obj interface{}) bool) {
	block, ok := p.block.Get(v)
	if ok == false {
		return
	}
	block.(*Chain).Range(func(k, v interface{}) bool {
		return f(k)
	})
}

//Get Возвращает данные из ячеики
func (p *MultiList) removeBlock(v Coord) {
	p.block.Del(v)
}

//newBlock newBlock
func (p *MultiList) newBlock(v Coord) *Chain {
	ch := NewChain()
	p.block.Set(v, ch)
	return ch
}

//Coord GetCoordinate(v)
func (p *MultiList) Coord(v vector.V) Coord {
	return Coord(v.Division(p.size).ToHash())
}
