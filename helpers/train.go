package helpers

import (
	"fmt"
	"sync"
)

//Структура данных Locomotive
//Содержит первый вагон и кол-во вагонов
type Locomotive struct {
	count int
	first *Carriage
	mu    *sync.Mutex
}

func NewLocomotive() *Locomotive {
	return &Locomotive{mu: &sync.Mutex{}}
}
func (l *Locomotive) Len() int {
	return l.count
}
func (l *Locomotive) String() string {
	s := ""
	l.Range(func(c *Carriage) bool {
		s += fmt.Sprintf(" %d ", c.GetCargo())
		return true
	})
	return fmt.Sprintf("Loco: len(%d), {%s}", l.Len(), s)
}

//Range Проходим по всем вагонам
func (l *Locomotive) Range(f func(c *Carriage) bool) {
	l.mu.Lock()
	defer l.mu.Unlock()
	if l.first == nil {
		return
	}
	c := l.first
	for {
		if f(c) == false {
			return
		}
		if c.next == nil {
			return
		}
		c = c.next
	}
}

func (l *Locomotive) add(c *Carriage) {
	l.mu.Lock()
	if l.first != nil {
		l.first.prev = c
	}
	c.next = l.first
	c.train = l
	l.first = c
	l.count++
	l.mu.Unlock()
}

func (l *Locomotive) del(c *Carriage) {
	l.mu.Lock()
	//Удаляем единственный вагон
	if l.count == 1 {
		l.first = nil
	}
	//Удаляем из середины состава
	if c.prev != nil && c.next != nil {
		c.prev.next = c.next
		c.next.prev = c.prev
	}
	//Удаляем в начале состава
	if c.prev == nil && c.next != nil {
		l.first = c.next
		c.next.prev = nil
	}
	//Удаляем в конце состава
	if c.next == nil && c.prev != nil {
		c.prev.next = nil
	}
	c.next = nil
	c.prev = nil
	c.train = nil
	l.count--
	l.mu.Unlock()
}

//Carriage Вагон с данными
type Carriage struct {
	train *Locomotive
	prev  *Carriage
	next  *Carriage
	cargo interface{}
}

//NewCarriage Новый вагон
func NewCarriage(cargo interface{}) *Carriage {
	return &Carriage{cargo: cargo}
}

//GetCargo Возвращает груз
func (c *Carriage) GetCargo() interface{} {
	return c.cargo
}

//GetTrain Возвращает поезд
func (c *Carriage) GetTrain() *Locomotive {
	return c.train
}

//Connect Присоединить к поезду
//Отключается от предыдущего поезда
func (c *Carriage) Connect(train *Locomotive) {
	if c.train != nil {
		c.train.del(c)
	}
	c.train = train
	train.add(c)
}

//Disconnect Отсоединить от поезда
func (c *Carriage) Disconnect() {
	if c.train == nil {
		return
	}
	c.train.del(c)
}
