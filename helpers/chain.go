package helpers

import (
	"sync"
)

type T interface {
}

//Chain - цепь даных
type Chain struct {
	l     *sync.Map
	first *links
	last  *links
	count int
	//m     *sync.Mutex
}

type links struct {
	prev, next *links
	key        T
	data       T
}

//New Создает цепочку данных
func NewChain() *Chain {
	c := &Chain{}
	c.l = &sync.Map{}
	c.count = 0
	//c.m = &sync.Mutex{}
	return c
}

//Set Доюавляет данные по ключу
func (c *Chain) Set(key T, data T) {
	// c.m.Lock()
	// defer c.m.Unlock()
	//проверка наличия ключа
	t, ok := c.l.Load(key)
	if ok == true {
		l := t.(*links)
		l.data = data
		return
	}
	c.count++

	//В конец очереди
	if c.first != nil && c.last != nil {
		c.last = newLinks(key, data, c.last, nil)
		c.last.prev.next = c.last
		c.l.Store(key, c.last)
		return
	}
	//Первый ключ
	if c.first == nil {
		c.first = newLinks(key, data, nil, nil)
		c.l.Store(key, c.first)
		c.last = nil
		return
	}
	//Второй ключ
	if c.first != nil && c.last == nil {
		c.last = newLinks(key, data, c.first, nil)
		c.first.next = c.last
		c.l.Store(key, c.last)
		return
	}

}

//Get Возвращает данные по ключу
func (c *Chain) Get(key T) (T, bool) {
	// c.m.Lock()
	// defer c.m.Unlock()
	t, ok := c.l.Load(key)
	if ok == true {
		return t.(*links).data, true
	}
	return nil, false
}

//Del Удаляет данные по ключу
func (c *Chain) Del(key T) {
	// c.m.Lock()
	// defer c.m.Unlock()
	if c.first == nil {
		return
	}
	t, ok := c.l.Load(key)
	if ok == false {
		//fmt.Printf("Ключ не найден: %p\n", key)
		return
	}
	//Удаляемое звено
	l := t.(*links)
	c.l.Delete(key)
	c.count--
	//Последнее звено(останется пустая цепь)
	if c.count == 0 {
		c.first = nil
		c.last = nil
		return
	}
	//Предпоследнее1
	if c.count == 1 && l.prev != nil {
		c.last = nil
		c.first.next = nil
		return
	}
	//Предпоследнее2
	if c.count == 1 && l.next != nil {
		c.first = l.next
		c.first.prev = nil
		c.last = nil
		return
	}
	//Первое звено в цепи
	if l.prev == nil && l.next != nil {
		c.first = l.next
		c.first.prev = nil
		return
	}
	//Последнее звено в цепи
	if l.next == nil && l.prev != nil {
		c.last = l.prev
		c.last.next = nil
		return
	}
	//Средняя часть цепи
	if l.next != nil && l.prev != nil {
		l.prev.next = l.next // закрываем разрыв цепи
		l.next.prev = l.prev
	}
}

//GetFirst Первый элемент цепи (key, data, true)
func (c *Chain) GetFirst() (T, T, bool) {
	// c.m.Lock()
	// defer c.m.Unlock()
	if c.first == nil {
		return nil, nil, false
	}
	return c.first.key, c.first.data, true
}

//Len Длина
func (c *Chain) Len() int {
	return c.count
}

//Range Перебираем цепочку от начала до конца в порядке добавления
func (c *Chain) Range(f func(k, v interface{}) bool) {
	if c.first == nil {
		return
	}
	l := c.first
	for {
		data, ok := c.Get(l.key)
		if ok != true {
			return
		}
		if f(l.key, data) == false {
			return
		}
		if l.next == nil {
			return
		}
		l = l.next
	}
}

//СomparisonKey Сравнение элементов (каждое с каждым)
func (c *Chain) СomparisonKey(f func(key1, data1, key2, data2 interface{}) bool) {
	if c.first == nil {
		return
	}
	l1 := c.first
	l2 := c.first.next
	for {
		data1, ok := c.Get(l1.key)
		if ok != true {
			break
		}
		for {
			data2, ok := c.Get(l2.key)
			if ok != true {
				break
			}
			if f(l1.key, data1, l2.key, data2) == false {
				return
			}
			if l2.next == nil {
				break
			}
			l2 = l2.next
		}
		if l1.next == nil {
			return
		}
		l1 = l1.next
	}
}

func newLinks(key T, data T, prev, next *links) *links {
	return &links{key: key, data: data, prev: prev, next: next}
}
