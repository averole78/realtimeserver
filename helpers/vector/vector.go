package vector

import (
	"fmt"
	"math"
	"realtimeserver/protos"

	"github.com/faiface/pixel"
)

//V vector 2d
type V [2]float64
type Hash int64

const RadToDeg = 180 / math.Pi
const X = 0
const Y = 1

var (
	// Zero holds a zero vector.
	Zero = V{}

	// UnitX holds a vector with X set to one.
	UnitX = V{1, 0}
	// UnitY holds a vector with Y set to one.
	UnitY = V{0, 1}
	// UnitXY holds a vector with X and Y set to one.
	UnitXY = V{1, 1}
	//Unit45 Нормализованный вектор под углом 45градусов
	Unit45 = V{0.7071067811865476, 0.7071067811865476}
	// MinVal holds a vector with the smallest possible component values.
	MinVal = V{-math.MaxFloat64, -math.MaxFloat64}
	// MaxVal holds a vector with the highest possible component values.
	MaxVal = V{+math.MaxFloat64, +math.MaxFloat64}
)

//NewVector   Новые вектор
func NewVector(x float64, y float64) *V {
	return &V{X: x, Y: y}
}

//Copy   Копия вектора
func (vec *V) Copy() *V {
	return &V{vec[X], vec[Y]}
}

// //Copied   Копия вектора
// func (vec *V) Copied() V {
// 	return *vec
// }

//Floor   Округление в меньшую сторону
func (vec *V) Floor() *V {
	vec[X] = math.Floor(vec[X])
	vec[Y] = math.Floor(vec[Y])
	return vec
}

// String formats V as string. See also Parse().
func (vec *V) String() string {
	return fmt.Sprintf("V(%v, %v)", vec[X], vec[Y])
}

//VectorToAngle360 Вектор преобразовывает в угол
func (vec *V) VectorToAngle360() float64 {
	a := math.Atan2(vec[1], vec[0]) * RadToDeg
	if a < 0 {
		a = 360 + a
	}
	return a
}

// StepFloor Округление до найменьшей Step
func (vec *V) StepFloor(step float64) *V {
	return vec.Division(step).Floor().Scale(step)
}

// ToHash Создает int64 на основе чисел вектора
// чисел вектора не должны превышать int32
func (vec *V) ToHash() Hash {
	return Hash(int64(vec[X])<<32 + int64(vec[Y]))
}

// ToProto convert to protos.Vector2D
func (vec *V) ToProto() *protos.Vector2D {
	return &protos.Vector2D{X: float32(vec[X]), Y: float32(vec[Y])}
}

// FromProto convert from protos.Vector2D
func FromProto(v *protos.Vector2D) *V {
	return &V{X: float64(v.X), Y: float64(v.Y)}
}

// ToPixel convert to pixel.Vec
func (vec *V) ToPixel() pixel.Vec {
	return pixel.Vec{X: vec[X], Y: vec[Y]}
}

// IsZero checks if all elements of the vector are zero
func (vec *V) IsZero() bool {
	return vec[X] == 0 && vec[Y] == 0
}

//GetX return float64
func (vec *V) GetX() float64 {
	return vec[X]
}

//GetY return float64
func (vec *V) GetY() float64 {
	return vec[Y]
}

//AddX float64
func (vec *V) AddX(x float64) *V {
	vec[X] += x
	return vec
}

//AddY float64
func (vec *V) AddY(y float64) *V {
	vec[Y] += y
	return vec
}

//AddXY float64
func (vec *V) AddXY(value float64) *V {
	vec[X] += value
	vec[Y] += value
	return vec
}

//X change
func (vec *V) X(x float64) *V {
	vec[X] = x
	return vec
}

//Y change
func (vec *V) Y(y float64) *V {
	vec[Y] = y
	return vec
}

// Length returns the length of the vector.
// See also LengthSqr and Normalize.
func (vec *V) Length() float64 {
	return math.Hypot(vec[X], vec[Y])
}

// LengthSqr returns the squared length of the vector.
// See also Length and Normalize.
func (vec *V) LengthSqr() float64 {
	return vec[X]*vec[X] + vec[Y]*vec[Y]
}

// Scale multiplies all element of the vector by f and returns vec.
func (vec *V) Scale(f float64) *V {
	vec[X] *= f
	vec[Y] *= f
	return vec
}

// // Scaled returns a copy of vec with all elements multiplies by f.
// func (vec *V) Scaled(f float64) V {
// 	return V{vec[X] * f, vec[Y] * f}
// }

// Division Division all element of the vector by f and returns vec.
func (vec *V) Division(f float64) *V {
	vec[X] /= f
	vec[Y] /= f
	return vec
}

// // Divisioned returns a copy of vec with all elements Division by f.
// func (vec *V) Divisioned(f float64) V {
// 	return V{vec[X] / f, vec[Y] / f}
// }

//Truncate Обрезает длину вектора до максимального значения
func (vec *V) Truncate(max float64) *V {
	if vec.Length() > max {
		return vec.Normalize().Scale(max)
	}
	return vec
}

// //Truncated Обрезает длину вектора до максимального значения
// func (vec *V) Truncated(max float64) V {
// 	v := *vec
// 	if vec.Length() > max {
// 		return v.Normalize().Scaled(max)
// 	}
// 	return v
// }

// Invert inverts the vector.
func (vec *V) Invert() *V {
	vec[X] = -vec[X]
	vec[Y] = -vec[Y]
	return vec
}

// //Inverted returns an inverted copy of the vector.
// func (vec *V) Inverted() V {
// 	return V{-vec[X], -vec[Y]}
// }

// Normalize normalizes the vector to unit length.
func (vec *V) Normalize() *V {
	sl := vec.LengthSqr()
	if sl == 0 || sl == 1 {
		return vec
	}
	return vec.Scale(1 / math.Sqrt(sl))
}

// // Normalized returns a unit length normalized copy of the vector.
// func (vec *V) Normalized() V {
// 	v := *vec
// 	v.Normalize()
// 	return v
// }

// Add adds another vector to vec.
func (vec *V) Add(v *V) *V {
	vec[X] += v[X]
	vec[Y] += v[Y]
	return vec
}

// Sub subtracts another vector from vec.
func (vec *V) Sub(v *V) *V {
	vec[X] -= v[X]
	vec[Y] -= v[Y]
	return vec
}

// Mul multiplies the components of the vector with the respective components of v.
func (vec *V) Mul(v *V) *V {
	vec[X] *= v[X]
	vec[Y] *= v[Y]
	return vec
}

// RoundN Округляет координаты до ближайшего кратного N числа.
func (vec *V) RoundN(n float64) *V {
	vec[X] = RoundN(vec[X], n)
	vec[Y] = RoundN(vec[Y], n)
	return vec
}

// RoundRose Округляет координаты до ближайшего кратного N числа.
func (vec *V) RoundRose(n float64) *V {
	vec[X] = RoundRose(vec[X], n)
	vec[Y] = RoundRose(vec[Y], n)
	return vec
}

// Rotate rotates the vector counter-clockwise by angle.
func (vec *V) Rotate(angle float64) *V {
	return vec.Rotated(angle)
}

// Rotated returns a counter-clockwise rotated copy of the vector.
func (vec *V) Rotated(angle float64) *V {
	sinus := math.Sin(angle)
	cosinus := math.Cos(angle)
	return &V{
		vec[X]*cosinus - vec[Y]*sinus,
		vec[X]*sinus + vec[Y]*cosinus,
	}
}

// RotateAroundPoint rotates the vector counter-clockwise around a point.
func (vec *V) RotateAroundPoint(point *V, angle float64) *V {
	return vec.Sub(point).Rotate(angle).Add(point)
}

// Rotate90DegLeft rotates the vector 90 degrees left (counter-clockwise).
func (vec *V) Rotate90DegLeft() *V {
	vec[X], vec[Y] = -vec[Y], vec[X]
	return vec
}

// Rotate90DegRight rotates the vector 90 degrees right (clockwise).
func (vec *V) Rotate90DegRight() *V {
	vec[X], vec[Y] = vec[Y], -vec[X]
	return vec
}

// Angle returns the counter-clockwise angle of the vector from the x axis.
func (vec *V) Angle() float64 {
	return math.Atan2(vec[Y], vec[X])
}

//
// Add returns the sum of two vectors.
func Add(a, b *V) V {
	return V{a[X] + b[X], a[Y] + b[Y]}
}

// Sub returns the difference of two vectors.
func Sub(a, b *V) V {
	return V{a[X] - b[X], a[Y] - b[Y]}
}

// Mul returns the component wise product of two vectors.
func Mul(a, b *V) V {
	return V{a[X] * b[X], a[Y] * b[Y]}
}

// Dot returns the dot product of two vectors.
func Dot(a, b *V) float64 {
	return a[X]*b[X] + a[Y]*b[Y]
}

// Cross returns the cross product of two vectors.
func Cross(a, b *V) V {
	return V{
		a[Y]*b[X] - a[X]*b[Y],
		a[X]*b[Y] - a[Y]*b[X],
	}
}

// Angle returns the angle between two vectors.
func Angle(a, b *V) float64 {
	v := Dot(a, b) / (a.Length() * b.Length())
	// prevent NaN
	if v > 1. {
		v = v - 2
	} else if v < -1. {
		v = v + 2
	}
	return math.Acos(v)
}

// IsLeftWinding returns if the angle from a to b is left winding.
func IsLeftWinding(a, b *V) bool {
	ab := b.Rotated(-a.Angle())
	return ab.Angle() > 0
}

// IsRightWinding returns if the angle from a to b is right winding.
func IsRightWinding(a, b *V) bool {
	ab := b.Rotated(-a.Angle())
	return ab.Angle() < 0
}

// GetMin возвращает компонентный минимум двух векторов.
func GetMin(a, b *V) V {
	min := *a
	if b[X] < min[X] {
		min[X] = b[X]
	}
	if b[Y] < min[Y] {
		min[Y] = b[Y]
	}
	return min
}

//  GetMax возвращает компонентный максимум из двух векторов
func GetMax(a, b *V) V {
	max := *a
	if b[X] > max[X] {
		max[X] = b[X]
	}
	if b[Y] > max[Y] {
		max[Y] = b[Y]
	}
	return max
}

// Interpolate интерполирует между a и b при t (0,1).
func Interpolate(a, b *V, t float64) V {
	t1 := 1 - t
	return V{
		a[X]*t1 + b[X]*t,
		a[Y]*t1 + b[Y]*t,
	}
}

// Clamp зажимает компоненты вектора в диапазоне от мин до макс.
func (vec *V) Clamp(min, max *V) *V {
	for i := range vec {
		if vec[i] < min[i] {
			vec[i] = min[i]
		} else if vec[i] > max[i] {
			vec[i] = max[i]
		}
	}
	return vec
}

// // Clamped возвращает копию вектора с зажатыми компонентами в диапазоне от мин до макс.
// func (vec *V) Clamped(min, max *V) V {
// 	result := *vec
// 	result.Clamp(min, max)
// 	return result
// }

// Clamp01 зажимает компоненты вектора в диапазоне от 0 до 1.
func (vec *V) Clamp01() *V {
	return vec.Clamp(&Zero, &UnitXY)
}

// // Clamped01 возвращает копию вектора с зажатыми компонентами в диапазоне от 0 до 1
// func (vec *V) Clamped01() V {
// 	result := *vec
// 	result.Clamp01()
// 	return result
// }

// Lerp returns a linear interpolation between vectors a and b.
//
// This function basically returns a point along the line between a and b and t chooses which one.
// If t is 0, then a will be returned, if t is 1, b will be returned. Anything between 0 and 1 will
// return the appropriate point between a and b and so on.
func Lerp(a, b *V, t float64) *V {
	return a.Scale(1 - t).Add(b.Scale(t))
}

//RoundN Округляет до ближайшего unit числа
func RoundN(x, unit float64) float64 {
	return math.Round(x/unit) * unit
}

//RoundRose Округляет до ближайшего unit числа
//быстрее в 2 раза чем RoundN
//Сам придумал))))
func RoundRose(x, unit float64) float64 {
	//return (math.Floor((x + (unit * 0.5)) / unit)) * unit
	//return math.Floor((x + (unit * 0.5)) / unit) * unit
	t := math.Floor(x/unit) * unit
	if x-t > unit*0.5 {
		return t + unit
	}
	return t
}
