package vector

type Path struct {
	slice []*V
}

//NewPath Новый путь
func NewPath() *Path {
	return &Path{make([]*V, 0, 4)}
}

//GetPoint GetPoint
func (p *Path) GetPoint(index int) *V {
	return p.slice[index]
}

//Len длина пути в точках
func (p *Path) Len() int {
	return len(p.slice)
}

//AddPoint Добавить точку
func (p *Path) AddPoint(v *V) {
	p.slice = append(p.slice, v)
}

//AddPoints Добавить точки
func (p *Path) AddPoints(v ...*V) {
	p.slice = append(p.slice, v...)
}

//Move Сместить путь
func (p *Path) Move(delta *V) {
	for _, v := range p.slice {
		v.Add(delta)
	}
}

//Range Перебрать путь
func (p *Path) Range(f func(index int, v *V) bool) {
	for i, v := range p.slice {
		if f(i, v) == false {
			return
		}
	}
}
