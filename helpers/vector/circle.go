package vector

import (
	"fmt"
	"math"
)

//Square Квадрат из 4 векторов

//Circle окружность
type Circle struct {
	*V
	r float64
}

//NewCircle Новый на основе позиции и радиуса
func NewCircle(pos *V, radius float64) *Circle {
	c := &Circle{}
	c.V = pos
	c.r = radius
	return c
}

//Radius Радиус
func (c *Circle) Radius() float64 {
	return c.r
}

//Position Position
func (c *Circle) Position() *V {
	return c.V
}

//Сopy Копирование
func (c *Circle) Сopy(delta *V) *Circle {
	return &Circle{V: c.V.Copy(), r: c.r}
}

//Move Перемещение
func (c *Circle) Move(delta *V) *Circle {
	c.V.Add(delta)
	return c
}

//String   fmt.Println(r) // Circle(100, 50, 200)
func (c *Circle) String() string {
	return fmt.Sprintf("Circle(%v, %v, %v)", c.V[X], c.V[Y], c.r)
}

//Rect Новый квадрат на основе позиции и радиуса
func (c *Circle) Rect() *Rect {
	r := &Rect{}
	t := Unit45.Copy().Scale(c.r * math.Sqrt2)
	r[Max] = c.V.Copy().Add(t)
	r[Min] = c.V.Copy().Add(t.Invert())
	return r
}
