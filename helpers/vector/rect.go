package vector

//Square Квадрат из 4 векторов
import (
	"fmt"
	"math"
	"math/rand"
)

const (
	Min = 0
	Max = 1
)

type Rect [2]*V

//RectFromCircle Новый на основе позиции и радиуса
func RectFromCircle(pos *V, radius float64) *Rect {
	r := &Rect{}
	t := Unit45.Copy().Scale(radius * math.Sqrt2)
	r[Max] = pos.Copy().Add(t)
	r[Min] = pos.Copy().Add(t.Invert())
	return r
}

//RectInVector Нахождение прямоугольника в плоскости
//клеток со сторонами равные step
func RectInVector(v *V, step float64) *Rect {
	r := &Rect{}
	r[Min] = v.StepFloor(step)
	r[Max] = r[Min].Copy().AddXY(step)
	return r
}

//Сopy Копирование
func (r *Rect) Сopy(delta *V) *Rect {
	return &Rect{r[Min].Copy(), r[Max].Copy()}
}

//Move Перемещение
func (r *Rect) Move(delta *V) *Rect {
	r[Min].Add(delta)
	r[Max].Add(delta)
	return r
}

//RandPoint Случайная точка находящаяся в плоскости прямойгольника
func (r *Rect) RandPoint() *V {
	return &V{rand.Float64()*r.W() + r.Min()[X], rand.Float64()*r.H() + r.Min()[Y]}
}

// String returns the string representation of the Rect.
//
//   r := pixel.R(100, 50, 200, 300)
//   r.String()     // returns "Rect(100, 50, 200, 300)"
//   fmt.Println(r) // Rect(100, 50, 200, 300)
func (r *Rect) String() string {
	return fmt.Sprintf("Rect(%v, %v, %v, %v)", r[Min][X], r[Min][Y], r[Max][X], r[Max][Y])
}

//Min Нижний левый вектор
func (r *Rect) Min() *V {
	return r[Min]
}

//Max Верхний правый вектор
func (r *Rect) Max() *V {
	return r[Max]
}

//W returns the width of the Rect.
func (r *Rect) W() float64 {
	return r[Max][X] - r[Min][X]
}

//H returns the height of the Rect.
func (r *Rect) H() float64 {
	return r[Max][Y] - r[Min][Y]
}

//Area returns the area of r. If r is not normalized, area may be negative.
func (r *Rect) Area() float64 {
	return r.W() * r.H()
}

// Center returns the position of the center of the Rect.
func (r *Rect) Center() *V {
	return Lerp(r[Min], r[Max], 0.5)
}

// Vertices returns a slice of the four corners which make up the rectangle.
func (r *Rect) Vertices() [4]*V {
	return [4]*V{
		r[Min],
		&V{r[Min][X], r[Max][Y]},
		r[Max],
		&V{r[Max][X], r[Min][Y]},
	}
}

func (r *Rect) ToPath() *Path {
	return &Path{
		slice: []*V{
			r[Min],
			&V{r[Min][X], r[Max][Y]},
			r[Max],
			&V{r[Max][X], r[Min][Y]},
		}}
}
