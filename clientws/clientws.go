package main

import (
	"crypto/md5"
	//"crypto/tls"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net/url"
	"realtimeserver/core"
	"realtimeserver/protos"
	"time"

	"github.com/golang/protobuf/proto"

	"github.com/gorilla/websocket"
)

var uid string

//var addr = flag.String("addr", "avergame.com:7778", "http service address")
var addr = flag.String("addr", "localhost:8000", "http service address")

//Key Key
type Key [md5.Size]byte

//GetKey GetKey
func GetKey() Key {
	k := md5.Sum([]byte("vycyciyc"))
	return Key(k)
}

func main() {
	flag.Parse()
	log.SetFlags(0)
	rand.Seed(time.Now().UnixNano())
	uid = "123" //fmt.Sprintf("%d", rand.Int63())
	fmt.Printf("Key:%x", GetKey())
	u := url.URL{Scheme: "ws", Host: *addr, Path: "/cl/" + uid}

	log.Printf("connecting to %s", u.String())

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)

	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()
	done := make(chan struct{})
	var method int
	go func() {
		for {
			_, _ = fmt.Scan(&method)
			send(c, protos.TypeMethod(method))
		}
	}()
	go listen(c)
	for {
		select {
		case <-done:
			return
		}
	}
}

//
func send(c *websocket.Conn, method protos.TypeMethod) {
	var err error
	switch method {
	case protos.TypeMethod_WORLD_LOAD:
		a := &protos.Empty{}
		b, e := proto.Marshal(a)
		if e != nil {
			return
		}
		b = append(b, byte(protos.TypeMethod_WORLD_LOAD))
		err = c.WriteMessage(websocket.BinaryMessage, b)
		log.Println("send TypeMethod_WORLD_LOAD")

	case protos.TypeMethod_UNIT_ADD:
		a := &protos.Empty{}
		b, e := proto.Marshal(a)
		if e != nil {
			return
		}
		b = append(b, byte(protos.TypeMethod_UNIT_ADD))
		err = c.WriteMessage(websocket.BinaryMessage, b)
		log.Println("send TypeMethod_UNIT_ADD")

	default:
		fmt.Println("API метод не найден")
	}

	if err != nil {
		log.Println("error write:", err)
		return
	}
}

func listen(c *websocket.Conn) {
	for {
		var err error
		_, raw, err := c.ReadMessage()

		if err != nil {
			log.Println("read error:", err)
			return
		}

		m, b := core.FromRaw(raw)
		//var mes server.Message
		switch protos.TypeMethod(m) {

		case protos.TypeMethod_UNIT_CREATE:
			log.Printf("UNIT_CREATE:  %s", len(b))
		case protos.TypeMethod_WORLD_LOAD:
			log.Printf("WORLD_LOAD:  %s", len(b))

		default:
			log.Printf("default: Неизвестная функция")
		}
	}
}
