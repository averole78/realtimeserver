package main

import (
	"flag"
	"math/rand"
	"realtimeserver/core"
	"time"

	"github.com/faiface/pixel/pixelgl"
)

var rout *core.Router

var portWS string
var managerClients *core.ManagerClients
var world *core.GameWorld

//Проверять
//go run -race https://4gophers.ru/articles/konkurentnost-v-go-gonki/#.XBtVXHVl-Cg

func main() {
	rand.Seed(int64(time.Now().Nanosecond()))

	//считываем  параметры запуска
	flag.StringVar(&portWS, "portWS", ":8001", "Server ws listen port.")
	flag.Parse()

	world = core.NewWorld()
	managerClients = core.NewManagerClients(world)
	rout = core.NewRouter(portWS, managerClients)
	go rout.Run()
	go world.Run()

	display := core.NewDisplay(world)
	pixelgl.Run(display.Run)
}
